package com.xm.recommendation.converter;

import com.xm.recommendation.enums.Crypto;
import com.xm.recommendation.exception.CryptoConversionException;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CryptoEnumConverter implements Converter<String, Crypto> {
    @Override
    public Crypto convert(String crypto) {
        try {
            return Crypto.valueOf(crypto.toUpperCase());
        } catch (Exception e) {
            throw new CryptoConversionException(String.format("Crypto %s currently is not supported", crypto));
        }
    }
}