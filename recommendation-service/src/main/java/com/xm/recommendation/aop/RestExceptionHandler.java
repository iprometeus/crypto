package com.example.ec.web;

import com.xm.recommendation.exception.CryptoConversionException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpServerErrorException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Handles Exceptions for controllers
 */
@RestControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(CryptoConversionException.class)
    public void handleConversionException(CryptoConversionException ex, HttpServletResponse res) throws IOException {
        res.sendError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
    }

    @ExceptionHandler(HttpServerErrorException.class)
    public void handleHttpServerErrorException(HttpServerErrorException ex, HttpServletResponse res) throws IOException {
        res.sendError(ex.getStatusCode().value(), ex.getMessage());
    }
}
