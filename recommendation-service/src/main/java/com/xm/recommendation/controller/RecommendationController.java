package com.xm.recommendation.controller;

import com.xm.recommendation.domain.PriceValues;
import com.xm.recommendation.enums.Crypto;
import com.xm.recommendation.service.RecommendationService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/recommendation")
public class RecommendationController {

    private final RecommendationService recommendationService;

    public RecommendationController(RecommendationService recommendationService) {
        this.recommendationService = recommendationService;
    }

    @GetMapping("/normalized-price-rank")
    public List<Crypto> getNormalizedPriceRank() {
        return recommendationService.getNormalizedPriceRank();

    }

    @GetMapping("/highest-normalized-price")
    public Crypto getHighestNormalizedCrypto(LocalDate date) {
        return recommendationService.getHighestNormalizedCrypto(date);

    }

    @GetMapping("/price-values/{crypto}")
    public PriceValues getPriceValues(@PathVariable("crypto") Crypto crypto) {
        return recommendationService.getPriceValues(crypto);
    }

}
