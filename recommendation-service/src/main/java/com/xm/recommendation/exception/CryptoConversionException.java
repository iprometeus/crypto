package com.xm.recommendation.exception;

public class CryptoConversionException extends RuntimeException {
    private final String message;

    public CryptoConversionException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
