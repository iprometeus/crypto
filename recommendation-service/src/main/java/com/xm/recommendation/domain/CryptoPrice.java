package com.xm.recommendation.domain;

import com.xm.recommendation.enums.Crypto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

public class CryptoPrice {

    private Crypto crypto;
    private BigDecimal price;
    private LocalDateTime dateTime;

    public Crypto getCrypto() {
        return crypto;
    }

    public void setCrypto(Crypto crypto) {
        this.crypto = crypto;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CryptoPrice that = (CryptoPrice) o;
        return crypto == that.crypto && price.equals(that.price) && dateTime.equals(that.dateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(crypto, price, dateTime);
    }
}
