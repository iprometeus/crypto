package com.xm.recommendation.domain;

import java.math.BigDecimal;

public class PriceValues {

    private BigDecimal crypto;

    private BigDecimal oldest;

    private BigDecimal newest;

    private BigDecimal min;

    private BigDecimal max;

    public BigDecimal getCrypto() {
        return crypto;
    }

    public void setCrypto(BigDecimal crypto) {
        this.crypto = crypto;
    }

    public BigDecimal getOldest() {
        return oldest;
    }

    public void setOldest(BigDecimal oldest) {
        this.oldest = oldest;
    }

    public BigDecimal getNewest() {
        return newest;
    }

    public void setNewest(BigDecimal newest) {
        this.newest = newest;
    }

    public BigDecimal getMin() {
        return min;
    }

    public void setMin(BigDecimal min) {
        this.min = min;
    }

    public BigDecimal getMax() {
        return max;
    }

    public void setMax(BigDecimal max) {
        this.max = max;
    }

}
