package com.xm.recommendation.service;

import com.xm.recommendation.domain.PriceValues;
import com.xm.recommendation.enums.Crypto;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class RecommendationService {

    public List<Crypto> getNormalizedPriceRank() {
        return new ArrayList<>();
    }

    public Crypto getHighestNormalizedCrypto(LocalDate date) {
        return Crypto.ETH;
    }

    public PriceValues getPriceValues(Crypto crypto) {
        return new PriceValues();
    }

}
