### recommendation-service *(not fully implemented yet)*
- Reads all the prices from the csv files
- Calculates oldest/newest/min/max for each crypto for the whole month
- Exposes an endpoint that will return a descending sorted list of all the cryptos,
comparing the normalized range (i.e. (max-min)/min)
- Exposes an endpoint that will return the oldest/newest/min/max values for a requested
crypto
- Exposes an endpoint that will return the crypto with the highest normalized range for a
specific day
### api-gateway
Is based on Spring Cloud Gateway. Is used for routing requests based on a number of criteria as well as for request rate limiting (not implemented yet).
### naming-server
Is based on Netflix Eureka service registry. All micro services including Recommendation service are registered into the Eureka server and it knows all the client applications running on each port and IP address.

## Run application
In root folder run comand **docker-compose up** to run containers.\
Links:
- http://localhost:8765/recommendation/price-values/ETH *(recommendation-service through api-gateway)*
- http://localhost:8000/recommendation/price-values/ETH *(direct GET call to recommendation-service)*
- http://localhost:8761/ *(Eureka)*
